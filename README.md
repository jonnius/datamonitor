# dataMonitor

This is a first conceptual app for UBports - Ubuntu Touch OS to bring to this platform the data usage monitoring capability, exploiting the QNetworkConfigurationManager API.

The present app is still in alpha development stage and its current version is 0.1.5.

!!!WARNING: the dataMonitor app works UNCONFINED, due to the need of getting the data statistics from the Network and to the employment of a daemon service, which is designed to execute the Actiondaemon binary every a determined time step, in order to have the received Mbytes database always up to date.

The received Mbytes database is automatically generated once installed the daemon files and run the Actiondaemon binary for the first time (after phone re-boot). The daemon runs in background, so it could be possible that it could eventually have an impact on battery discharge rate (I will run some tests later to prove the actual impact). If you want to stay on the safe side and you want to avoid any remote battery drainage, you can delete the daemon files only by opening the Settings page and tapping on the 'remove daemon' button; remember that every time you open the dataMonitor app again, the daemon files will be automatically restored back.

Useful directories:

- app main directory is /opt/click.ubuntu.com/datamonitor.matteobellei;

- mattdaemon.conf and mattdaemon-service.conf are stored in /.config/upstart;

- mattdaemon.log and mattdaemon-service.log are stored in /.cache/upstart;

- received Mbytes database is stored in /.local/share/datamonitor.matteobellei/Databases;

- additional service .conf files are stored in /.config/datamonitor.matteobellei.

Change logs:

-Version 0.1.7-

1) Fixed bug which prevented the app to work for the SIM connection (feedback needed on this).

2) Daemon .conf files modified in a more useful and simple way. To get rid of the previous .conf versions, user to go to 'Settings', to push 'remove daemon', to exit from the app and open the app again to have the new .conf files copied at the .config/upstart location.

3) Added new settings in the 'Settings' page:
    1. Switch to enable colors by theme global selection or to be custom set; user can now set a custom color + opacity for the background, lines, font, buttons, graph grid and bars;
    2. Switch + button to enable the global theme selection by daylight (between Ambiance and SuruDark only), setting a threshold time;
    3. Slider to custom set the daemon time step for its idle activity, to better control the battery energy drain.

4) Minor bug fixes.


-Version 0.1.6-

1) A new icon has been introduced with the present release (I'm not a great artist so this could be a temporary solution in case somebody else would come up with a better idea);
2) Added notifications in case of data thresholds exceeding. The code employs the C++ function 'popen' which apparently is the only one I discovered to work seamlessly without killing the daemon ('system' and 'QProcess' were the other two methods tried before);
3) Added summary at the Main.qml page for SIM and Wi-fi data;
4) Added the "info" section on the up right corner, close to "settings";
5) Merged early translation to spanish thanks to @Krakakanov (new additional strings to be translated have been added since then);
6) Added translation to italian thanks to...me;
7) Added gestures to interact with the graph:
    1. Scroll down/up with two fingers to enlarge/narrow the 'MBytes' data range;
    2. Press and hold with one finger to create a new data threshold object;
8) Added 'BottomEdge' section to store the list of the created data thresholds. In this section you can:
    1. Drag the data threshold row towards right, to delete the data threshold;
    2. Drag the data threshold row towards left, to mute the notification for the concerned threshold;
9) Added labels for 'Mbytes' and 'Days of Month';
10) Fixed 'Mbytes' data steps to be consistent between the Vertical and Horizontal view and to fill all the space at its disposal;
11) Fixed bug occurring when the date passed from one day to the day after, not calculating the received data in a correct way;
12) Added message on the middle of the page to inform about data unavailability when opening a graph with no data yet;
13) Added message on the middle of the 'BottomEdge' page to inform about thresholds unavailability when no data thresholds yet.
