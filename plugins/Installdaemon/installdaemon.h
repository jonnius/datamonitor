#ifndef INSTALLDAEMON_H
#define INSTALLDAEMON_H

#include <QObject>
#include <QProcess>

class Installdaemon: public QObject {
    Q_OBJECT

public:
    Installdaemon();
    ~Installdaemon() = default;

    Q_INVOKABLE void install();
    Q_INVOKABLE void uninstall();

    Q_INVOKABLE void storeTimeStep(const QString tstep);

Q_SIGNALS:
    void installed(bool success);
    void uninstalled(bool success);

    void isInstalledChanged(const bool isInstalled);

private Q_SLOTS:
    void onInstallFinished(int exitCode, QProcess::ExitStatus exitStatus);
    void onUninstallFinished(int exitCode, QProcess::ExitStatus exitStatus);

private:

    bool checkInstalled();

    QProcess m_installProcess;
    QProcess m_uninstallProcess;
    bool m_isInstalled = false;
};


#endif
