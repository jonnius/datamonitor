#include <QDebug>
#include <QCoreApplication>
#include <QFileInfo>
#include <iostream>
#include <fstream>
#include <QString>

#include "installdaemon.h"

using namespace std;

Installdaemon::Installdaemon() :
    m_installProcess(),
    m_uninstallProcess()
{
    connect(&m_installProcess, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(onInstallFinished(int, QProcess::ExitStatus)));
    connect(&m_uninstallProcess, SIGNAL(finished(int, QProcess::ExitStatus)), this, SLOT(onUninstallFinished(int, QProcess::ExitStatus)));

    checkInstalled();
}

void Installdaemon::install() {
    //TODO don't hardcode this
    m_installProcess.start("bash /opt/click.ubuntu.com/datamonitor.matteobellei/current/daemon/install.sh");
}

void Installdaemon::uninstall() {
    //TODO don't hardcode this
    m_uninstallProcess.start("bash /opt/click.ubuntu.com/datamonitor.matteobellei/current/daemon/uninstall.sh");
}

void Installdaemon::onInstallFinished(int exitCode, QProcess::ExitStatus exitStatus) {
    qDebug() << "install finished";
    qDebug() << "stdout" << m_installProcess.readAllStandardOutput();
    qDebug() << "stderr" << m_installProcess.readAllStandardError();
    qDebug() << "exit code" << exitCode << "exit status" << exitStatus;

    checkInstalled();
    Q_EMIT installed(exitCode == 0 && exitStatus == QProcess::NormalExit);
}

void Installdaemon::onUninstallFinished(int exitCode, QProcess::ExitStatus exitStatus) {
    qDebug() << "uninstall finished";

    qDebug() << "stdout" << m_uninstallProcess.readAllStandardOutput();
    qDebug() << "stderr" << m_uninstallProcess.readAllStandardError();
    qDebug() << "exit code" << exitCode << "exit status" << exitStatus;

    checkInstalled();
    Q_EMIT uninstalled(exitCode == 0 && exitStatus == QProcess::NormalExit);
}

bool Installdaemon::checkInstalled() {
    QFileInfo session("/home/phablet/.config/upstart/mattdaemon.conf");

//    m_isInstalled = session.exists() && indicator.exists();
    m_isInstalled = session.exists();
    Q_EMIT isInstalledChanged(m_isInstalled);

    return m_isInstalled;
}

void Installdaemon::storeTimeStep(const QString tstep) {
  string tstepString = tstep.toStdString();
  string command = "echo ";
  command = command + tstepString + " > /home/phablet/.config/datamonitor.matteobellei/daemonIdle.conf";
  FILE* pipe = popen(command.c_str(), "r");
}
/*  string tstepstr = tstep.toUtf8().constData();
  cout << tstepstr;
  fstream tstepFile;
  tstepFile.open(".config/datamonitor.matteobellei/daemonIdle.conf",ios::in);
  if(!tstepFile) {
    qDebug()<<"Error in reading daemonIdle.conf file..";
    tstepFile.open(".config/datamonitor.matteobellei/daemonIdle.conf",fstream::out);  // line needed to create the file for the first time
    tstepFile<<tstepstr;
    qDebug()<<"File .config/datamonitor.matteobellei/daemonIdle.conf created.";
  } else {
    tstepFile.close();
    tstepFile.open(".config/datamonitor.matteobellei/daemonIdle.conf",fstream::out);
    tstepFile<<tstepstr;
    tstepFile.close();
  } */
//}
