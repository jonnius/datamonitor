import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
//import Ubuntu.PushNotifications 0.1
import Installdaemon 1.0

import QtQuick.LocalStorage 2.0
import "Storage.js" as Storage
import "DateUtils.js" as DateUtils

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'datamonitor.matteobellei'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

//    property string themeColor
    property string globalTheme: Theme.name;
    property string gridColor;
    property string bkgColor;
    property string textColor;
    property string fontColor;
    property string lineColor;
    property string buttonColor;
    property string barColor;
    property string nowTime
    property double todayStoredSIM : 0;
    property double totalStoredSIM : 0;
    property double todayStoredWIFI : 0;
    property double totalStoredWIFI : 0;

    Settings {
        id:settings
//        category: "MainApp"
        /* flag to show or not the App configuration popup */
//        property bool isFirstUse : true;
        property bool isDaemonInstalled : false;
        property double maxScaleY : 0;
        property double maxScaleYStored : 0;
        property double maxScaleYSIM : 0;
        property double maxScaleYSIMStored : 0;
        property double axisX : 0;
        property double axisY : 0;
        property double errorX : 0;
        property double errorY1 : 0;
        property double errorY2 : 0;
        property string dataType : "SIM";
        property bool themeSelection : true;
        property bool themeSwitchByTime : false;
        property string backgrndColor: "#a9a9a9";
        property string gridColor: "#000000";
        property string fontColor: "#ffff00";
        property string lineColor: "#ffff00";
        property string buttonColor: "#606060";
        property string barColor: "#ffff00";
        property string backgrndColorCust: "#606060";
        property string gridColorCust: "#000000";
        property string fontColorCust: "#ffff33";
        property string lineColorCust: "#ffff33";
        property string buttonColorCust: "#000000";
        property string barColorCust: "#ff00ff";
        property real opac1: 1;
        property real opac2: 0.15;
        property real opac3: 1;
        property real opac4: 1;
        property real opac5: 1;
        property real opac6: 0.3;
        property real enableDaemonTime: 0;
        property real daemonIdleTime: 40;
        property string labelTime: "40 sec";
        property bool lockedPalette: true;
        property string daylightSwitch: "18 00";
    }

    Rectangle {
        color: bkgColor
        width: parent.width
        height: parent.height
    }

    Component {
       id: settingPage
       PrefPage{}
    }

    Component {
       id: aboutPage
       AboutPage{}
    }

    Component {
       id: chartPage
       ChartPage{}
    }

    PageStack {
        id: pageStack

        /* set the first page of the application */
        Component.onCompleted: {
            pageStack.push(mainPage);
        }

    Page {
        id: mainPage
        anchors.fill: parent

        header: PageHeader {
            id: pageHeader
            title: i18n.tr('Data usage monitor')

            trailingActionBar.actions: [
              Action {
                  text: i18n.tr("Settings")
                  onTriggered: pageStack.push(settingPage)
                  iconName: "settings"
              },
              Action {
                  text: i18n.tr("About")
                  onTriggered: pageStack.push(aboutPage)
                  iconName: "info"
              }
            ]

            StyleHints	{
              foregroundColor:	UbuntuColors.orange
//              backgroundColor:	UbuntuColors.porcelain
              backgroundColor: bkgColor
              dividerColor:	UbuntuColors.slate
              }
            }
            Component.onCompleted: {
                if(!settings.isDaemonInstalled)
                {
                    message.visible = false;
                    Installdaemon.install();
                    if(Installdaemon.isInstalled) {
                      settings.isDaemonInstalled=false;
                    }
                    else {
                      settings.isDaemonInstalled=true;
                    }
                }
                themeColorSelection();
                todayStoredSIM = todayData("SIM").toFixed(1)
                totalStoredSIM = totalData("SIM").toFixed(1);
                todayStoredWIFI = todayData("WIFI").toFixed(1);
                totalStoredWIFI = totalData("WIFI").toFixed(1);
            }

            Rectangle {
                color: bkgColor
                width: parent.width
                height: parent.height
            }

      ScrollView {
          id: scrollView
          anchors {
              top: mainPage.header.bottom
              bottom: parent.bottom
              left: parent.left
              right: parent.right
          }

        Column {
            width: scrollView.width
            Row{
                id:simChartRow
                Button {
                    id:showSIMChartButton
                    text: i18n.tr("SIM data chart")
                    color: buttonColor
                    width: units.gu(20)
                    onClicked: {
                        settings.dataType = "SIM";
                        pageStack.push(chartPage)
                    }
                }
            }
            Row{
                Rectangle {
                    color: "transparent"
                    width: units.gu(10)
                    height: units.gu(3)
                }
            }
            Column{
                Row{
                    Label{
                        id: todayDataSIM
                        text: "     " + i18n.tr("Today received data") + " (MBytes) ="
                        textSize: Label.Medium
                        color: fontColor
                    }
                    Rectangle {
                        color: "transparent"
                        width: units.gu(2)
                        height: units.gu(3)
                    }
                    TextMetrics {
                      id: textMetrics1
                      text: todayStoredSIM
                    }
                    Rectangle {
                      color: "transparent"
                      border.color: lineColor
                      border.width: 2
                      width: textMetrics1.advanceWidth + units.gu(2)
                      height: units.gu(3)
                      Label{
                          id: todayDataSIM2
                          anchors.centerIn: parent
                          text: todayStoredSIM
                          textSize: Label.Large
                          color: fontColor
                      }
                    }
                }
            }
            Row{
                Rectangle {
                    color: "transparent"
                    width: units.gu(10)
                    height: units.gu(5)
                }
            }
            Column{
                Row{
                  Label{
                      id: totalDataSIM
                      text: "     " + i18n.tr("Month total received data")  + " (MBytes) ="
                      textSize: Label.Medium
                      color: fontColor
                  }
                  Rectangle {
                      color: "transparent"
                      width: units.gu(2)
                      height: units.gu(3)
                  }
                  TextMetrics {
                    id: textMetrics2
                    text: totalStoredSIM
                  }
                  Rectangle {
                    color: "transparent"
                    border.color: lineColor
                    border.width: 2
                    width: textMetrics2.advanceWidth + units.gu(2)
                    height: units.gu(3)
                    Label{
                        id: totalDataSIM2
                        anchors.centerIn: parent
                        text: totalStoredSIM
                        textSize: Label.Large
                        color: fontColor
                    }
                  }
                }
            }
            Row{
                Rectangle {
                    color: "transparent"
                    width: units.gu(10)
                    height: units.gu(10)
                }
            }
            Row{
                id:chartRow
                Button {
                    id:showChartButton
                    text: i18n.tr("Wi-fi data chart")
                    color: buttonColor
                    width: units.gu(20)
                    onClicked: {
                        settings.dataType = "WIFI";
                        pageStack.push(chartPage)
                    }
                }
            }
            Row{
                Rectangle {
                    color: "transparent"
                    width: units.gu(10)
                    height: units.gu(3)
                }
            }
            Column{
                Row{
                    Label{
                        id: todayDataWIFI
                        text: "     " + i18n.tr("Today received data")  + " (MBytes) ="
                        textSize: Label.Medium
                        color: fontColor
                    }
                    Rectangle {
                        color: "transparent"
                        width: units.gu(2)
                        height: units.gu(3)
                    }
                    TextMetrics {
                      id: textMetrics3
                      text: todayStoredWIFI
                    }
                    Rectangle {
                      color: "transparent"
                      border.color: lineColor
                      border.width: 2
                      width: textMetrics3.advanceWidth + units.gu(2)
                      height: units.gu(3)
                      Label{
                          id: todayDataWIFI2
                          anchors.centerIn: parent
                          text: todayStoredWIFI
                          textSize: Label.Large
                          color: fontColor
                      }
                    }
                }
            }
            Row{
                Rectangle {
                    color: "transparent"
                    width: units.gu(10)
                    height: units.gu(5)
                }
            }
            Column{
                Row{
                    Label{
                        id: totalDataWIFI
                        text: "     " + i18n.tr("Month total received data")  + " (MBytes) ="
                        textSize: Label.Medium
                        color: fontColor
                    }
                    Rectangle {
                        color: "transparent"
                        width: units.gu(2)
                        height: units.gu(3)
                    }
                    TextMetrics {
                      id: textMetrics4
                      text: totalStoredWIFI
                    }
                    Rectangle {
                      color: "transparent"
                      border.color: lineColor
                      border.width: 2
                      width: textMetrics4.advanceWidth + units.gu(2)
                      height: units.gu(3)
                      Label{
                          id: totalDataWIFI2
                          anchors.centerIn: parent
                          text: totalStoredWIFI
                          textSize: Label.Large
                          color: fontColor
                      }
                    }
                }
            }
            Row{
              Label {
                id: message
                horizontalAlignment: Text.AlignLeft
                wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                visible: false
                width: mainPage.width
                height: units.gu(30)
              }
            }
         }
    }
    }
  }
  Connections {
    target: Installdaemon

    onInstalled: {
        message.visible = true;
        if (success) {
            message.text = i18n.tr("Daemon automatically installed.\nPlease reboot the phone to set the data usage monitoring application online.");
            message.color = UbuntuColors.green;
        }
        else {
            message.text = i18n.tr("Failed to install");
            message.color = UbuntuColors.red;
        }
    }
  }

  function themeColorSelection() {
  if (settings.themeSwitchByTime) {
    nowTime = new Date()
    var time = Qt.formatDateTime(nowTime, "hh mm")
    if (time>=settings.daylightSwitch) {
      var selectedTheme="Ubuntu.Components.Themes.SuruDark"
    } else {
      var selectedTheme="Ubuntu.Components.Themes.Ambiance"
    }
  } else {
    var selectedTheme=globalTheme
  }

  switch(settings.themeSelection) {
  case true:
      switch(selectedTheme) {
      case "Ubuntu.Components.Themes.Ambiance":
            settings.backgrndColor = "white"
            settings.gridColor = Qt.rgba(0,0,0, 0.05)
            settings.fontColor = "#666"
            settings.lineColor = Qt.rgba(0,0,0, 0.3)
            settings.buttonColor = "#606060"
            settings.barColor = Qt.rgba(0.2,0.2,0.2, 0.5)
            break;
      case "Ubuntu.Components.Themes.SuruDark":
            settings.backgrndColor = "black"
            settings.gridColor = Qt.rgba(1,1,1, 0.1)
            settings.fontColor = "white"
            settings.lineColor = "white"
            settings.buttonColor = "#606060"
            settings.barColor = Qt.rgba(0.86,0.86,0.86, 0.5)
            break;
      case "Ubuntu.Components.Themes.SuruGradient":
            settings.backgrndColor = "purple"
            settings.gridColor = Qt.rgba(1,0,1, 0.1)
            settings.fontColor = Qt.rgba(1,1,0, 1)
            settings.lineColor = Qt.rgba(1,0,1, 1)
            settings.buttonColor = "#ffff33"
            settings.barColor = Qt.rgba(0.86,0.86,0.86, 0.5)
            break;
      }
      bkgColor = settings.backgrndColor
      gridColor = settings.gridColor
      fontColor = settings.fontColor
      lineColor = settings.lineColor
      buttonColor = settings.buttonColor
      barColor = settings.barColor
      break;
  case false:
      bkgColor = settings.backgrndColorCust
      gridColor = settings.gridColorCust
      fontColor = settings.fontColorCust
      lineColor = settings.lineColorCust
      buttonColor = settings.buttonColorCust
      barColor = settings.barColorCust
      break;
  }
  }

  function todayData(conntype) {
    var actual_date = Qt.formatDateTime(new Date(), "dd MMMM yyyy");
    var yesterday = DateUtils.addDaysAndFormat(actual_date, -1)
    var yesterdayData = Storage.getBytesValueByDate(yesterday, conntype)
    var firstOfMonth = DateUtils.firstOfMonth(actual_date);
		var daysToFirst = DateUtils.getDifferenceInDays(firstOfMonth,actual_date);
    var todayData = totalData(conntype)
    if (daysToFirst==0) {
        yesterdayData = 0
    }
    var sumData = todayData-yesterdayData
    if (sumData<0) {
      sumData=0}
    return sumData
  }

  function totalData(conntype) {
    var actual_date = Qt.formatDateTime(new Date(), "dd MMMM yyyy");
    var todayTotalData = Storage.getBytesValueByDate(actual_date, conntype)
    if (todayTotalData==0) {
      var yesterday = DateUtils.addDaysAndFormat(actual_date, -1)
      var yesterdayData = Storage.getBytesValueByDate(yesterday, conntype)
      todayTotalData=yesterdayData
    }
    return todayTotalData
  }
}
