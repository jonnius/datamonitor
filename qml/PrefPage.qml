import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import Installdaemon 1.0
import Ubuntu.Components.Pickers 1.3

Page {
    id: prefPage

    property int numberColors: 9
    property int paletteSize: (scrollView.width/numberColors<100) ? scrollView.width/numberColors : 100
    property int maxVertFontSize: 30
    property int maxHorizFontSize: 34
    property real minOpc: 0.05
    property string color1: Qt.rgba(0,0,0)                      //black
    property string color2: Qt.rgba(0,0,1)                      //blue
    property string color3: Qt.rgba(0.376471,0.376471,0.376471) //grey
    property string color4: Qt.rgba(0,0.6,0)                    //green
    property string color5: Qt.rgba(1,0,0)                      //red
    property string color6: Qt.rgba(1,0,1)                      //fuchsia
    property string color7: Qt.rgba(0,1,1)                      //skyblue
    property string color8: Qt.rgba(1,1,0.2)                    //yellow
    property string color9: Qt.rgba(1,1,1)                      //white
    property string colorTracker: Qt.rgba(1,0.5,0)
    property string borderColorTracker: Qt.rgba(0,0,0.9)

    header: PageHeader {
        id: pageHeader
        title: i18n.tr("Settings")

        StyleHints	{
          foregroundColor: fontColor
          backgroundColor: bkgColor
          dividerColor:	UbuntuColors.slate
        }

    }

    Rectangle {
        color: bkgColor
        width: parent.width
        height: parent.height
    }

    Component.onCompleted: {
      switcherThemes.checkedChanged()
    }

    ScrollView {
        id: scrollView
        anchors {
            top: pageHeader.bottom
            bottom: parent.bottom
            left: parent.left
            right: parent.right
            leftMargin: units.gu(2)
            rightMargin: units.gu(2)
        }

        clip: true

        Column {
              spacing: units.gu(2)
              width: scrollView.width

                  Column {
                        Row {
                          Rectangle {
                              color: "transparent"
                              width: units.gu(10)
                              height: units.gu(3)
                          }
                        }
                        Row{
                            Label {
                               id: enableThemesColorStyle
                               width: prefPage.width -2*switcherThemes.width
                               wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                               color: fontColor
                               height: units.gu(10)
                               font.pointSize: prefPage.width/1920*40<maxVertFontSize ? maxVertFontSize : prefPage.width/1920*maxHorizFontSize
                               text: i18n.tr("Enable global Theme style from OS general settings")
                             }
                             Switch {
                                  id: switcherThemes
                                  checked: settings.themeSelection
                                  onCheckedChanged: {
                                       settings.themeSelection=switcherThemes.checked;
                                       if (checked) {
                                          enableThemeColorDayTime.visible=true
                                          enableThemeColorDayTime.height = units.gu(10)
                                          switcherTimeTheme.visible=true
                                          if (switcherTimeTheme.checked) {
                                            targetTimeSelectorButton.visible=true
                                          } else {
                                            targetTimeSelectorButton.visible=false
                                          }
                                          topPaletteFrame.visible=false
                                          labelPalettes.visible=false
                                          lockColors.visible=false
                                          backgroundPalette.visible=false
                                          space1.visible=false
                                          label1.visible=false
                                          fontOpacity.visible=false
                                          sliderBkg.visible=false
                                          space2.visible=false
                                          label2.visible=false
                                          gridPalette.visible=false
                                          fontOpacity2.visible=false
                                          sliderGrid.visible=false
                                          space3.visible=false
                                          label3.visible=false
                                          fontPalette.visible=false
                                          fontOpacity3.visible=false
                                          sliderFont.visible=false
                                          space4.visible=false
                                          label4.visible=false
                                          linePalette.visible=false
                                          fontOpacity4.visible=false
                                          sliderLine.visible=false
                                          label6.visible=false
                                          buttonPalette.visible=false
                                          fontOpacity6.visible=false
                                          sliderButton.visible=false
                                          space5.visible=false
                                          label5.visible=false
                                          barPalette.visible=false
                                          fontOpacity5.visible=false
                                          sliderBar.visible=false
                                       } else {
                                          enableThemeColorDayTime.visible=false
                                          enableThemeColorDayTime.height=0
                                          targetTimeSelectorButton.visible=false
                                          switcherTimeTheme.visible=false
                                          topPaletteFrame.visible=true
                                          labelPalettes.visible=true
                                          lockColors.visible=true
                                          backgroundPalette.visible=true
                                          space1.visible=true
                                          label1.visible=true
                                          fontOpacity.visible=true
                                          sliderBkg.visible=true
                                          space2.visible=true
                                          label2.visible=true
                                          gridPalette.visible=true
                                          fontOpacity2.visible=true
                                          sliderGrid.visible=true
                                          space3.visible=true
                                          label3.visible=true
                                          fontPalette.visible=true
                                          fontOpacity3.visible=true
                                          sliderFont.visible=true
                                          space4.visible=true
                                          label4.visible=true
                                          linePalette.visible=true
                                          fontOpacity4.visible=true
                                          sliderLine.visible=true
                                          label6.visible=true
                                          buttonPalette.visible=true
                                          fontOpacity6.visible=true
                                          sliderButton.visible=true
                                          space5.visible=true
                                          label5.visible=true
                                          barPalette.visible=true
                                          fontOpacity5.visible=true
                                          sliderBar.visible=true
                                       }
                                  }
                                }
                        }
                        Row{
                            Label {
                               id: enableThemeColorDayTime
                               width: targetTimeSelectorButton.visible ? prefPage.width - switcherTimeTheme.width*2 - targetTimeSelectorButton.width : prefPage.width - switcherTimeTheme.width*2
                               wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                               color: fontColor
                               font.pointSize: prefPage.width/1920*40<maxVertFontSize ? maxVertFontSize : prefPage.width/1920*maxHorizFontSize
                               text: i18n.tr("Enable global Theme to follow the day light (SuruDark or Ambiance themes) by setting the time for the switch")
                             }
                             Component {
                                 id: popoverTargetTimePicker
                                 Popover {
                                     id: popoverTimePicker

                                     DatePicker {
                                         id: timePicker
                                         mode: "Hours|Minutes"
                                         /* when Datepicker is closed, is updated the date shown in the button */
                                         Component.onDestruction: {
                                             targetTimeSelectorButton.text = Qt.formatDateTime(timePicker.date, "hh mm");
                                             settings.daylightSwitch=targetTimeSelectorButton.text
                                         }
                                     }
                                 }
                             }
                             Button {
                                 id: targetTimeSelectorButton
                                 width: units.gu(10)
                                 text: settings.daylightSwitch
                                 onClicked: {
                                     PopupUtils.open(popoverTargetTimePicker, targetTimeSelectorButton)
                                 }
                             }
                             Switch {
                                  id: switcherTimeTheme
                                  checked: settings.themeSwitchByTime
                                  onCheckedChanged: {
                                       if (checked) {
                                            targetTimeSelectorButton.visible=true
                                          } else {
                                            targetTimeSelectorButton.visible=false
                                          }
                                       settings.themeSwitchByTime=switcherTimeTheme.checked
                                  }
                              }
                        }
                        Row {
                            Rectangle {
                                id: topPaletteFrame
                                color: "transparent"
                                border.color: lineColor
                                border.width: 1
                                width: scrollView.width
                                height: units.gu(0.1)
                            }
                        }
                        Row {
                            Label {
                                id: labelPalettes
                                color: fontColor
                                font.pointSize: enableThemeColorDayTime.font.pointSize + 5
                                verticalAlignment: Text.AlignBottom
                                width: scrollView.width - lockColors.width
                                height: units.gu(5)
                                font.underline: true
                                text: i18n.tr("Colors Palettes:")
                            }
                            Icon {
                              id: lockColors
                              name: settings.lockedPalette ? "lock" : "lock-broken"  //lock-broken settings.lockedPalette "lock"
                              height: units.gu(8)
                              width:   units.gu(8)
                              MouseArea {
                                  anchors.fill: parent
                                  onClicked: {
                                    if (settings.lockedPalette) {
                                      settings.lockedPalette=false
                                      lockColors.name="lock-broken"
                                      backgroundPalette.enabled=true
                                      sliderBkg.enabled=true
                                      fontPalette.enabled=true
                                      sliderFont.enabled=true
                                      linePalette.enabled=true
                                      sliderLine.enabled=true
                                      buttonPalette.enabled=true
                                      sliderButton.enabled=true
                                      gridPalette.enabled=true
                                      sliderGrid.enabled=true
                                      barPalette.enabled=true
                                      sliderBar.enabled=true
                                    } else {
                                      settings.lockedPalette=true
                                      lockColors.name="lock"
                                      backgroundPalette.enabled=false
                                      sliderBkg.enabled=false
                                      fontPalette.enabled=false
                                      sliderFont.enabled=false
                                      linePalette.enabled=false
                                      sliderLine.enabled=false
                                      buttonPalette.enabled=false
                                      sliderButton.enabled=false
                                      gridPalette.enabled=false
                                      sliderGrid.enabled=false
                                      barPalette.enabled=false
                                      sliderBar.enabled=false
                                    }
                                  }
                              }
                            }
                        }
                        Row {
                          Rectangle {
                              id: space1
                              color: "transparent"
                              width: units.gu(10)
                              height: units.gu(1)
                          }
                        }
                        Row {
                            Label {
                                id: label1
                                color: fontColor
                                font.pointSize: enableThemesColorStyle.font.pointSize
                                text: i18n.tr("Background color:")
                            }
                        }
                        Row {
                            Grid {
                                  id: backgroundPalette
                                  enabled: !settings.lockedPalette
                                  columns: numberColors
                                  rows: 1
                                  spacing: 10
                                  Repeater {
                                      id: repeater1
                                      model: numberColors
                                      delegate: PaletteColor {
                                                width: prefPage.paletteSize
                                                height: prefPage.paletteSize
                                                namePalette: "background"
                                                colorTracker: prefPage.colorTracker
                                                borderColorTracker: prefPage.borderColorTracker
                                                onResetColorSelection: {
                                                  for (var i=0; i <= backgroundPalette.columns-1; i++) {
                                                    repeater1.itemAt(i).colorSelectedRect="transparent"
                                                    repeater1.itemAt(i).colorBorderSelectedRect="transparent"
                                                  }
                                                }
                                                }
                                                onItemAdded: {
                                                      var opacQtrgba = Qt.rgba(0,0,0, settings.opac1)
                                                      switch (index) {
                                                      case 0: if (settings.backgrndColorCust==colorsBystrings(String(opacQtrgba), prefPage.color1)) {
                                                                itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                              }
                                                              break;
                                                      case 1: if (settings.backgrndColorCust==colorsBystrings(String(opacQtrgba), prefPage.color2)) {
                                                                itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                              }
                                                              break;
                                                      case 2: if (settings.backgrndColorCust==colorsBystrings(String(opacQtrgba), prefPage.color3)) {
                                                                itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                              }
                                                              break;
                                                      case 3: if (settings.backgrndColorCust==colorsBystrings(String(opacQtrgba), prefPage.color4)) {
                                                                itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                              }
                                                              break;
                                                      case 4: if (settings.backgrndColorCust==colorsBystrings(String(opacQtrgba), prefPage.color5)) {
                                                                itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                              }
                                                              break;
                                                      case 5: if (settings.backgrndColorCust==colorsBystrings(String(opacQtrgba), prefPage.color6)) {
                                                                itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                              }
                                                              break;
                                                      case 6: if (settings.backgrndColorCust==colorsBystrings(String(opacQtrgba), prefPage.color7)) {
                                                                itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                              }
                                                              break;
                                                      case 7: if (settings.backgrndColorCust==colorsBystrings(String(opacQtrgba), prefPage.color8)) {
                                                                itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                              }
                                                              break;
                                                      case 8: if (settings.backgrndColorCust==colorsBystrings(String(opacQtrgba), prefPage.color9)) {
                                                                itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                              }
                                                              break;
                                                      }
                                                }
                                      }
                                  }
                          }
                          Row {
                            Label {
                                id: fontOpacity
                                color: fontColor
                                height: units.gu(4.8)
                                font.pointSize: label1.font.pointSize - 4
                                verticalAlignment: Text.AlignVCenter
                                text: i18n.tr("Opacity:")
                            }
                            Slider {
                                id: sliderBkg
                                enabled: !settings.lockedPalette
                                function formatValue(v) {
                                      if (backgroundPalette.visible) {
                                        var opacQtrgba = Qt.rgba(0,0,0, v)
                                        repeater1.itemAt(0).color= colorsBystrings(String(opacQtrgba), prefPage.color1);
                                        repeater1.itemAt(1).color= colorsBystrings(String(opacQtrgba), prefPage.color2);
                                        repeater1.itemAt(2).color= colorsBystrings(String(opacQtrgba), prefPage.color3);
                                        repeater1.itemAt(3).color= colorsBystrings(String(opacQtrgba), prefPage.color4);
                                        repeater1.itemAt(4).color= colorsBystrings(String(opacQtrgba), prefPage.color5);
                                        repeater1.itemAt(5).color= colorsBystrings(String(opacQtrgba), prefPage.color6);
                                        repeater1.itemAt(6).color= colorsBystrings(String(opacQtrgba), prefPage.color7);
                                        repeater1.itemAt(7).color= colorsBystrings(String(opacQtrgba), prefPage.color8);
                                        repeater1.itemAt(8).color= colorsBystrings(String(opacQtrgba), prefPage.color9);
                                        for (var i=0; i <= backgroundPalette.columns-1; i++) {
                                          if (repeater1.itemAt(i).colorSelectedRect==prefPage.colorTracker) {
                                               settings.backgrndColorCust = repeater1.itemAt(i).color
                                          }
                                        }
                                        settings.opac1 = v
                                      }
                                      return v.toFixed(2)
                                }
                                minimumValue: minOpc
                                maximumValue: 1
                                value: settings.opac1
                                live: true
                            }
                          }
                          Row {
                            Rectangle {
                                id: space3
                                color: "transparent"
                                width: units.gu(5)
                                height: units.gu(1)
                            }
                          }
                          Row {
                              Label {
                                  id: label3
                                  color: fontColor
                                  font.pointSize: enableThemesColorStyle.font.pointSize
                                  text: i18n.tr("Font color:")
                              }
                          }
                          Row {
                              Grid {
                                    id: fontPalette
                                    enabled: !settings.lockedPalette
                                    columns: numberColors
                                    rows: 1
                                    spacing: 10
                                    Repeater {
                                        id: repeater3
                                        model: numberColors
                                        delegate: PaletteColor {
                                                    width: prefPage.paletteSize
                                                    height: prefPage.paletteSize
                                                    namePalette: "font"
                                                    colorTracker: prefPage.colorTracker
                                                    borderColorTracker: prefPage.borderColorTracker
                                                    onResetColorSelection: {
                                                      for (var i=0; i <= fontPalette.columns-1; i++) {
                                                        repeater3.itemAt(i).colorSelectedRect="transparent"
                                                        repeater3.itemAt(i).colorBorderSelectedRect="transparent"
                                                      }
                                                    }
                                                  }
                                                  onItemAdded: {
                                                      var opacQtrgba = Qt.rgba(0,0,0, settings.opac3)
                                                      switch (index) {
                                                            case 0: if (settings.fontColorCust==colorsBystrings(String(opacQtrgba), prefPage.color1)) {
                                                                      itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                      itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                    }
                                                                    break;
                                                            case 1: if (settings.fontColorCust==colorsBystrings(String(opacQtrgba), prefPage.color2)) {
                                                                      itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                      itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                    }
                                                                    break;
                                                            case 2: if (settings.fontColorCust==colorsBystrings(String(opacQtrgba), prefPage.color3)) {
                                                                      itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                      itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                    }
                                                                    break;
                                                            case 3: if (settings.fontColorCust==colorsBystrings(String(opacQtrgba), prefPage.color4)) {
                                                                      itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                      itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                    }
                                                                    break;
                                                            case 4: if (settings.fontColorCust==colorsBystrings(String(opacQtrgba), prefPage.color5)) {
                                                                      itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                      itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                    }
                                                                    break;
                                                            case 5: if (settings.fontColorCust==colorsBystrings(String(opacQtrgba), prefPage.color6)) {
                                                                      itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                      itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                    }
                                                                    break;
                                                            case 6: if (settings.fontColorCust==colorsBystrings(String(opacQtrgba), prefPage.color7)) {
                                                                      itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                      itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                    }
                                                                    break;
                                                            case 7: if (settings.fontColorCust==colorsBystrings(String(opacQtrgba), prefPage.color8)) {
                                                                      itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                      itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                    }
                                                                    break;
                                                            case 8: if (settings.fontColorCust==colorsBystrings(String(opacQtrgba), prefPage.color9)) {
                                                                      itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                      itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                    }
                                                                    break;
                                                            }
                                                  }
                                    }
                              }
                          }
                          Row {
                            Label {
                                id: fontOpacity3
                                color: fontColor
                                height: units.gu(4.8)
                                font.pointSize: label3.font.pointSize - 4
                                verticalAlignment: Text.AlignVCenter
                                text: fontOpacity.text
                            }
                            Slider {
                                id: sliderFont
                                enabled: !settings.lockedPalette
                                function formatValue(v) {
                                    if (fontPalette.visible) {
                                      var opacQtrgba = Qt.rgba(0,0,0, v)
                                      repeater3.itemAt(0).color= colorsBystrings(String(opacQtrgba), prefPage.color1);
                                      repeater3.itemAt(1).color= colorsBystrings(String(opacQtrgba), prefPage.color2);
                                      repeater3.itemAt(2).color= colorsBystrings(String(opacQtrgba), prefPage.color3);
                                      repeater3.itemAt(3).color= colorsBystrings(String(opacQtrgba), prefPage.color4);
                                      repeater3.itemAt(4).color= colorsBystrings(String(opacQtrgba), prefPage.color5);
                                      repeater3.itemAt(5).color= colorsBystrings(String(opacQtrgba), prefPage.color6);
                                      repeater3.itemAt(6).color= colorsBystrings(String(opacQtrgba), prefPage.color7);
                                      repeater3.itemAt(7).color= colorsBystrings(String(opacQtrgba), prefPage.color8);
                                      repeater3.itemAt(8).color= colorsBystrings(String(opacQtrgba), prefPage.color9);
                                      for (var i=0; i <= fontPalette.columns-1; i++) {
                                        if (repeater3.itemAt(i).colorSelectedRect==prefPage.colorTracker) {
                                             settings.fontColorCust = repeater3.itemAt(i).color
                                        }
                                      }
                                      settings.opac3 = v
                                    }
                                    return v.toFixed(2)
                                }
                                minimumValue: minOpc
                                maximumValue: 1
                                value: settings.opac3
                                live: true
                            }
                          }
                          Row {
                            Rectangle {
                                id: space4
                                color: "transparent"
                                width: units.gu(5)
                                height: units.gu(1)
                            }
                          }
                          Row {
                              Label {
                                  id: label4
                                  color: fontColor
                                  font.pointSize: enableThemesColorStyle.font.pointSize
                                  text: i18n.tr("Lines color:")
                              }
                          }
                          Row {
                              Grid {
                                    id: linePalette
                                    enabled: !settings.lockedPalette
                                    columns: numberColors
                                    rows: 1
                                    spacing: 10
                                    Repeater {
                                        id: repeater4
                                        model: numberColors
                                        delegate: PaletteColor {
                                                    width: prefPage.paletteSize
                                                    height: prefPage.paletteSize
                                                    namePalette: "line"
                                                    colorTracker: prefPage.colorTracker
                                                    borderColorTracker: prefPage.borderColorTracker
                                                    onResetColorSelection: {
                                                      for (var i=0; i <= linePalette.columns-1; i++) {
                                                        repeater4.itemAt(i).colorSelectedRect="transparent"
                                                        repeater4.itemAt(i).colorBorderSelectedRect="transparent"
                                                      }
                                                    }
                                                  }
                                                  onItemAdded: {
                                                      var opacQtrgba = Qt.rgba(0,0,0, settings.opac4)
                                                      switch (index) {
                                                            case 0: if (settings.lineColorCust==colorsBystrings(String(opacQtrgba), prefPage.color1)) {
                                                                      itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                      itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                    }
                                                                    break;
                                                            case 1: if (settings.lineColorCust==colorsBystrings(String(opacQtrgba), prefPage.color2)) {
                                                                      itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                      itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                    }
                                                                    break;
                                                            case 2: if (settings.lineColorCust==colorsBystrings(String(opacQtrgba), prefPage.color3)) {
                                                                      itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                      itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                    }
                                                                    break;
                                                            case 3: if (settings.lineColorCust==colorsBystrings(String(opacQtrgba), prefPage.color4)) {
                                                                      itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                      itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                    }
                                                                    break;
                                                            case 4: if (settings.lineColorCust==colorsBystrings(String(opacQtrgba), prefPage.color5)) {
                                                                      itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                      itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                    }
                                                                    break;
                                                            case 5: if (settings.lineColorCust==colorsBystrings(String(opacQtrgba), prefPage.color6)) {
                                                                      itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                      itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                    }
                                                                    break;
                                                            case 6: if (settings.lineColorCust==colorsBystrings(String(opacQtrgba), prefPage.color7)) {
                                                                      itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                      itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                    }
                                                                    break;
                                                            case 7: if (settings.lineColorCust==colorsBystrings(String(opacQtrgba), prefPage.color8)) {
                                                                      itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                      itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                    }
                                                                    break;
                                                            case 8: if (settings.lineColorCust==colorsBystrings(String(opacQtrgba), prefPage.color9)) {
                                                                      itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                      itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                    }
                                                                    break;
                                                            }
                                                  }
                                    }
                              }
                          }
                          Row {
                            Label {
                                id: fontOpacity4
                                color: fontColor
                                height: units.gu(4.8)
                                font.pointSize: label4.font.pointSize - 4
                                verticalAlignment: Text.AlignVCenter
                                text: fontOpacity.text
                            }
                            Slider {
                                id: sliderLine
                                enabled: !settings.lockedPalette
                                function formatValue(v) {
                                    if (linePalette.visible) {
                                      var opacQtrgba = Qt.rgba(0,0,0, v)
                                      repeater4.itemAt(0).color= colorsBystrings(String(opacQtrgba), prefPage.color1);
                                      repeater4.itemAt(1).color= colorsBystrings(String(opacQtrgba), prefPage.color2);
                                      repeater4.itemAt(2).color= colorsBystrings(String(opacQtrgba), prefPage.color3);
                                      repeater4.itemAt(3).color= colorsBystrings(String(opacQtrgba), prefPage.color4);
                                      repeater4.itemAt(4).color= colorsBystrings(String(opacQtrgba), prefPage.color5);
                                      repeater4.itemAt(5).color= colorsBystrings(String(opacQtrgba), prefPage.color6);
                                      repeater4.itemAt(6).color= colorsBystrings(String(opacQtrgba), prefPage.color7);
                                      repeater4.itemAt(7).color= colorsBystrings(String(opacQtrgba), prefPage.color8);
                                      repeater4.itemAt(8).color= colorsBystrings(String(opacQtrgba), prefPage.color9);
                                      for (var i=0; i <= linePalette.columns-1; i++) {
                                        if (repeater4.itemAt(i).colorSelectedRect==prefPage.colorTracker) {
                                             settings.lineColorCust = repeater4.itemAt(i).color
                                        }
                                      }
                                      settings.opac4 = v
                                    }
                                    return v.toFixed(2)
                                }
                                minimumValue: minOpc
                                maximumValue: 1
                                value: settings.opac4
                                live: true
                            }
                          }
                          Row {
                            Rectangle {
                                id: space2
                                color: "transparent"
                                width: units.gu(5)
                                height: units.gu(1)
                            }
                          }
                          Row {
                              Label {
                                  id: label6
                                  color: fontColor
                                  font.pointSize: enableThemesColorStyle.font.pointSize
                                  text: i18n.tr("Buttons color:")
                              }
                          }
                          Row {
                              Grid {
                                    id: buttonPalette
                                    enabled: !settings.lockedPalette
                                    columns: numberColors
                                    rows: 1
                                    spacing: 10
                                    Repeater {
                                        id: repeater6
                                        model: numberColors
                                        delegate: PaletteColor {
                                                    width: prefPage.paletteSize
                                                    height: prefPage.paletteSize
                                                    namePalette: "button"
                                                    colorTracker: prefPage.colorTracker
                                                    borderColorTracker: prefPage.borderColorTracker
                                                    onResetColorSelection: {
                                                      for (var i=0; i <= buttonPalette.columns-1; i++) {
                                                        repeater6.itemAt(i).colorSelectedRect="transparent"
                                                        repeater6.itemAt(i).colorBorderSelectedRect="transparent"
                                                      }
                                                    }
                                                  }
                                                  onItemAdded: {
                                                      var opacQtrgba = Qt.rgba(0,0,0, settings.opac6)
                                                      switch (index) {
                                                            case 0: if (settings.buttonColorCust==colorsBystrings(String(opacQtrgba), prefPage.color1)) {
                                                                      itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                      itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                    }
                                                                    break;
                                                            case 1: if (settings.buttonColorCust==colorsBystrings(String(opacQtrgba), prefPage.color2)) {
                                                                      itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                      itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                    }
                                                                    break;
                                                            case 2: if (settings.buttonColorCust==colorsBystrings(String(opacQtrgba), prefPage.color3)) {
                                                                      itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                      itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                    }
                                                                    break;
                                                            case 3: if (settings.buttonColorCust==colorsBystrings(String(opacQtrgba), prefPage.color4)) {
                                                                      itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                      itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                    }
                                                                    break;
                                                            case 4: if (settings.buttonColorCust==colorsBystrings(String(opacQtrgba), prefPage.color5)) {
                                                                      itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                      itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                    }
                                                                    break;
                                                            case 5: if (settings.buttonColorCust==colorsBystrings(String(opacQtrgba), prefPage.color6)) {
                                                                      itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                      itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                    }
                                                                    break;
                                                            case 6: if (settings.buttonColorCust==colorsBystrings(String(opacQtrgba), prefPage.color7)) {
                                                                      itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                      itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                    }
                                                                    break;
                                                            case 7: if (settings.buttonColorCust==colorsBystrings(String(opacQtrgba), prefPage.color8)) {
                                                                      itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                      itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                    }
                                                                    break;
                                                            case 8: if (settings.buttonColorCust==colorsBystrings(String(opacQtrgba), prefPage.color9)) {
                                                                      itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                      itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                    }
                                                                    break;
                                                            }
                                                  }
                                    }
                              }
                          }
                          Row {
                            Label {
                                id: fontOpacity6
                                color: fontColor
                                height: units.gu(4.8)
                                font.pointSize: label4.font.pointSize - 4
                                verticalAlignment: Text.AlignVCenter
                                text: fontOpacity.text
                            }
                            Slider {
                                id: sliderButton
                                enabled: !settings.lockedPalette
                                function formatValue(v) {
                                    if (buttonPalette.visible) {
                                      var opacQtrgba = Qt.rgba(0,0,0, v)
                                      repeater6.itemAt(0).color= colorsBystrings(String(opacQtrgba), prefPage.color1);
                                      repeater6.itemAt(1).color= colorsBystrings(String(opacQtrgba), prefPage.color2);
                                      repeater6.itemAt(2).color= colorsBystrings(String(opacQtrgba), prefPage.color3);
                                      repeater6.itemAt(3).color= colorsBystrings(String(opacQtrgba), prefPage.color4);
                                      repeater6.itemAt(4).color= colorsBystrings(String(opacQtrgba), prefPage.color5);
                                      repeater6.itemAt(5).color= colorsBystrings(String(opacQtrgba), prefPage.color6);
                                      repeater6.itemAt(6).color= colorsBystrings(String(opacQtrgba), prefPage.color7);
                                      repeater6.itemAt(7).color= colorsBystrings(String(opacQtrgba), prefPage.color8);
                                      repeater6.itemAt(8).color= colorsBystrings(String(opacQtrgba), prefPage.color9);
                                      for (var i=0; i <= buttonPalette.columns-1; i++) {
                                        if (repeater6.itemAt(i).colorSelectedRect==prefPage.colorTracker) {
                                             settings.buttonColorCust = repeater6.itemAt(i).color
                                        }
                                      }
                                      settings.opac6 = v
                                    }
                                    return v.toFixed(2)
                                }
                                minimumValue: minOpc
                                maximumValue: 1
                                value: settings.opac6
                                live: true
                            }
                          }
                          Row {
                            Rectangle {
                                id: space6
                                color: "transparent"
                                width: units.gu(5)
                                height: units.gu(1)
                            }
                          }
                          Row {
                              Label {
                                  id: label2
                                  color: fontColor
                                  font.pointSize: enableThemesColorStyle.font.pointSize
                                  text: i18n.tr("Grid color (Graph only):")
                              }
                          }
                          Row {
                              Grid {
                                    id: gridPalette
                                    enabled: !settings.lockedPalette
                                    columns: numberColors
                                    rows: 1
                                    spacing: 10
                                    Repeater {
                                        id: repeater2
                                        model: numberColors
                                        delegate: PaletteColor {
                                                  width: prefPage.paletteSize
                                                  height: prefPage.paletteSize
                                                  namePalette: "grid"
                                                  colorTracker: prefPage.colorTracker
                                                  borderColorTracker: prefPage.borderColorTracker
                                                  onResetColorSelection: {
                                                    for (var i=0; i <= gridPalette.columns-1; i++) {
                                                      repeater2.itemAt(i).colorSelectedRect="transparent"
                                                      repeater2.itemAt(i).colorBorderSelectedRect="transparent"
                                                    }
                                                  }
                                                  }
                                                  onItemAdded: {
                                                  var opacQtrgba = Qt.rgba(0,0,0, settings.opac2)
                                                  switch (index) {
                                                        case 0: if (settings.gridColorCust==colorsBystrings(String(opacQtrgba), prefPage.color1)) {
                                                                  itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                  itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                }
                                                                break;
                                                        case 1: if (settings.gridColorCust==colorsBystrings(String(opacQtrgba), prefPage.color2)) {
                                                                  itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                  itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                }
                                                                break;
                                                        case 2: if (settings.gridColorCust==colorsBystrings(String(opacQtrgba), prefPage.color3)) {
                                                                  itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                  itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                }
                                                                break;
                                                        case 3: if (settings.gridColorCust==colorsBystrings(String(opacQtrgba), prefPage.color4)) {
                                                                  itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                  itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                }
                                                                break;
                                                        case 4: if (settings.gridColorCust==colorsBystrings(String(opacQtrgba), prefPage.color5)) {
                                                                  itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                  itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                }
                                                                break;
                                                        case 5: if (settings.gridColorCust==colorsBystrings(String(opacQtrgba), prefPage.color6)) {
                                                                  itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                  itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                }
                                                                break;
                                                        case 6: if (settings.gridColorCust==colorsBystrings(String(opacQtrgba), prefPage.color7)) {
                                                                  itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                  itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                }
                                                                break;
                                                        case 7: if (settings.gridColorCust==colorsBystrings(String(opacQtrgba), prefPage.color8)) {
                                                                  itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                  itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                }
                                                                break;
                                                        case 8: if (settings.gridColorCust==colorsBystrings(String(opacQtrgba), prefPage.color9)) {
                                                                  itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                  itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                }
                                                                break;
                                                        }
                                                  }
                                        }
                                    }
                            }
                            Row {
                              Label {
                                  id: fontOpacity2
                                  color: fontColor
                                  height: units.gu(4.8)
                                  font.pointSize: label2.font.pointSize - 4
                                  verticalAlignment: Text.AlignVCenter
                                  text: fontOpacity.text
                              }
                              Slider {
                                  id: sliderGrid
                                  enabled: !settings.lockedPalette
                                  function formatValue(v) {
                                      if (gridPalette.visible) {
                                        var opacQtrgba = Qt.rgba(0,0,0, v)
                                        repeater2.itemAt(0).color= colorsBystrings(String(opacQtrgba), prefPage.color1);
                                        repeater2.itemAt(1).color= colorsBystrings(String(opacQtrgba), prefPage.color2);
                                        repeater2.itemAt(2).color= colorsBystrings(String(opacQtrgba), prefPage.color3);
                                        repeater2.itemAt(3).color= colorsBystrings(String(opacQtrgba), prefPage.color4);
                                        repeater2.itemAt(4).color= colorsBystrings(String(opacQtrgba), prefPage.color5);
                                        repeater2.itemAt(5).color= colorsBystrings(String(opacQtrgba), prefPage.color6);
                                        repeater2.itemAt(6).color= colorsBystrings(String(opacQtrgba), prefPage.color7);
                                        repeater2.itemAt(7).color= colorsBystrings(String(opacQtrgba), prefPage.color8);
                                        repeater2.itemAt(8).color= colorsBystrings(String(opacQtrgba), prefPage.color9);
                                        for (var i=0; i <= gridPalette.columns-1; i++) {
                                          if (repeater2.itemAt(i).colorSelectedRect==prefPage.colorTracker) {
                                               settings.gridColorCust = repeater2.itemAt(i).color
                                          }
                                        }
                                        settings.opac2 = v
                                      }
                                      return v.toFixed(2)
                                  }
                                  minimumValue: minOpc
                                  maximumValue: 1
                                  value: settings.opac2
                                  live: true
                              }
                            }
                            Row {
                              Rectangle {
                                  id: space5
                                  color: "transparent"
                                  width: units.gu(5)
                                  height: units.gu(1)
                              }
                            }
                            Row {
                                Label {
                                    id: label5
                                    color: fontColor
                                    font.pointSize: enableThemesColorStyle.font.pointSize
                                    text: i18n.tr("Bars fill color (Graph only):")
                                }
                            }
                            Row {
                                Grid {
                                      id: barPalette
                                      enabled: !settings.lockedPalette
                                      columns: numberColors
                                      rows: 1
                                      spacing: 10
                                      Repeater {
                                          id: repeater5
                                          model: numberColors
                                          delegate: PaletteColor {
                                                    width: prefPage.paletteSize
                                                    height: prefPage.paletteSize
                                                    namePalette: "bar"
                                                    colorTracker: prefPage.colorTracker
                                                    borderColorTracker: prefPage.borderColorTracker
                                                    onResetColorSelection: {
                                                      for (var i=0; i <= barPalette.columns-1; i++) {
                                                        repeater5.itemAt(i).colorSelectedRect="transparent"
                                                        repeater5.itemAt(i).colorBorderSelectedRect="transparent"
                                                      }
                                                    }
                                                    }
                                                    onItemAdded: {
                                                    var opacQtrgba = Qt.rgba(0,0,0, settings.opac5)
                                                    switch (index) {
                                                          case 0: if (settings.barColorCust==colorsBystrings(String(opacQtrgba), prefPage.color1)) {
                                                                    itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                    itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                  }
                                                                  break;
                                                          case 1: if (settings.barColorCust==colorsBystrings(String(opacQtrgba), prefPage.color2)) {
                                                                    itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                    itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                  }
                                                                  break;
                                                          case 2: if (settings.barColorCust==colorsBystrings(String(opacQtrgba), prefPage.color3)) {
                                                                    itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                    itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                  }
                                                                  break;
                                                          case 3: if (settings.barColorCust==colorsBystrings(String(opacQtrgba), prefPage.color4)) {
                                                                    itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                    itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                  }
                                                                  break;
                                                          case 4: if (settings.barColorCust==colorsBystrings(String(opacQtrgba), prefPage.color5)) {
                                                                    itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                    itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                  }
                                                                  break;
                                                          case 5: if (settings.barColorCust==colorsBystrings(String(opacQtrgba), prefPage.color6)) {
                                                                    itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                    itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                  }
                                                                  break;
                                                          case 6: if (settings.barColorCust==colorsBystrings(String(opacQtrgba), prefPage.color7)) {
                                                                    itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                    itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                  }
                                                                  break;
                                                          case 7: if (settings.barColorCust==colorsBystrings(String(opacQtrgba), prefPage.color8)) {
                                                                    itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                    itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                  }
                                                                  break;
                                                          case 8: if (settings.barColorCust==colorsBystrings(String(opacQtrgba), prefPage.color9)) {
                                                                    itemAt(index).colorSelectedRect=prefPage.colorTracker
                                                                    itemAt(index).colorBorderSelectedRect=prefPage.borderColorTracker
                                                                  }
                                                                  break;
                                                          }
                                                    }
                                          }
                                      }
                              }
                              Row {
                                Label {
                                    id: fontOpacity5
                                    color: fontColor
                                    height: units.gu(4.8)
                                    font.pointSize: label5.font.pointSize - 4
                                    verticalAlignment: Text.AlignVCenter
                                    text: fontOpacity.text
                                }
                                Slider {
                                    id: sliderBar
                                    enabled: !settings.lockedPalette
                                    function formatValue(v) {
                                        if (barPalette.visible) {
                                          var opacQtrgba = Qt.rgba(0,0,0, v)
                                          repeater5.itemAt(0).color= colorsBystrings(String(opacQtrgba), prefPage.color1);
                                          repeater5.itemAt(1).color= colorsBystrings(String(opacQtrgba), prefPage.color2);
                                          repeater5.itemAt(2).color= colorsBystrings(String(opacQtrgba), prefPage.color3);
                                          repeater5.itemAt(3).color= colorsBystrings(String(opacQtrgba), prefPage.color4);
                                          repeater5.itemAt(4).color= colorsBystrings(String(opacQtrgba), prefPage.color5);
                                          repeater5.itemAt(5).color= colorsBystrings(String(opacQtrgba), prefPage.color6);
                                          repeater5.itemAt(6).color= colorsBystrings(String(opacQtrgba), prefPage.color7);
                                          repeater5.itemAt(7).color= colorsBystrings(String(opacQtrgba), prefPage.color8);
                                          repeater5.itemAt(8).color= colorsBystrings(String(opacQtrgba), prefPage.color9);
                                          for (var i=0; i <= barPalette.columns-1; i++) {
                                            if (repeater5.itemAt(i).colorSelectedRect==prefPage.colorTracker) {
                                                 settings.barColorCust = repeater5.itemAt(i).color
                                            }
                                          }
                                          settings.opac5 = v
                                        }
                                        return v.toFixed(2)
                                    }
                                    minimumValue: minOpc
                                    maximumValue: 1
                                    value: settings.opac5
                                    live: true
                                }
                              }
                              Row {
                                  Rectangle {
                                      id: bottomPaletteFrame
                                      color: "transparent"
                                      border.color: lineColor
                                      border.width: 1
                                      width: scrollView.width
                                      height: units.gu(0.1)
                                  }
                              }
                              Label {
                                  id: daemonLabel
                                  color: fontColor
                                  font.pointSize: enableThemeColorDayTime.font.pointSize + 5
                                  verticalAlignment: Text.AlignVCenter
                                  width: scrollView.width
                                  height: units.gu(7)
                                  font.underline: true
                                  text: i18n.tr("Daemon Settings:")
                              }
                              Row{
                                  Label {
                                     id: enableDaemonIdleTime
                                     width: prefPage.width - units.gu(4)
                                     wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                                     color: fontColor
                                     font.pointSize: enableThemesColorStyle.font.pointSize
                                     text: i18n.tr("Set time step for daemon in idle activity (the smaller the worse for battery drain):")
                                  }
                              }
                              Row{
                                  Rectangle {
                                      color: "transparent"
                                      width: units.gu(5)
                                      height: units.gu(1)
                                  }
                                  Slider {
                                      id: sliderDaemonIdleTime
                                      width: scrollView.width - 2*units.gu(5)
                                      function formatValue(v) {
                                          if (v>=60) {
                                            var initime = v/60
                                            var min = Math.floor(initime)
                                            var sec = (initime-min)*60
                                            var secRnd = Math.round(sec)
                                            var time = min + " min and " + secRnd + " sec"
                                          } else {
                                            var time = v.toFixed(0) + " sec"
                                          }
                                          settings.daemonIdleTime =  Math.round(v)
                                          var daemonStep = settings.daemonIdleTime
                                          Installdaemon.storeTimeStep(String(daemonStep))
                                          settings.labelTime = time
                                          availTime.text = time
                                          return time
                                      }
                                      minimumValue: 10
                                      maximumValue: 600
                                      value: settings.daemonIdleTime
                                      live: true
                                  }
                                  Rectangle {
                                      color: "transparent"
                                      width: units.gu(5)
                                      height: units.gu(1)
                                  }
                              }
                              Row{
                                  Label {
                                     id: availTime
                                     height: units.gu(7)
                                     width: scrollView.width
                                     wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                                     color: fontColor
                                     font.pointSize: enableDaemonIdleTime.font.pointSize-3
                                     text: settings.labelTime
                                  }
                              }
                              Row{
                                  Label {
                                     width: prefPage.width -5/4*deleteDaemon.width
                                     wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                                     color: fontColor
                                     font.pointSize: enableDaemonIdleTime.font.pointSize
                                     text: i18n.tr("Remove daemon to save battery. This action will put offline the monitoring of received data")
                                   }
                                  Button {
                                      id: deleteDaemon
                                      text: i18n.tr("Remove daemon")
                                      color: buttonColor
                                      width: units.gu(17)
                                      onClicked: {
                                          if(settings.isDaemonInstalled)
                                          {
                                              message.visible = false;
                                              Installdaemon.uninstall();
                                              if(Installdaemon.isInstalled) {
                                                settings.isDaemonInstalled=true;
                                              }
                                              else {
                                                settings.isDaemonInstalled=false;
                                              }
                                          }
                                      }
                                  }
                              }
                              Row{
                                Label {
                                  id: message
                                  horizontalAlignment: Text.AlignLeft
                                  wrapMode: Text.WrapAtWordBoundaryOrAnywhere
                                  visible: false
                                  width: prefPage.width-units.gu(4)
                                  height: units.gu(10)
                                }
                              }
                    }
         }
     }
     Connections {
       target: Installdaemon

       onUninstalled: {
           message.visible = true;
           if (success) {
               message.text = i18n.tr("Daemon files removed.\nDataMonitor is offline. Close and open again the 'dataMonitor' app to get daemon files automatically installed.");
               message.color = UbuntuColors.red;
           }
           else {
               message.text = i18n.tr("Failed to uninstall both the daemon files");
               message.color = UbuntuColors.green;
           }
       }
     }
     function colorsBystrings(opacity, color) {
        var opacitySL = opacity.slice(0, 3);
        var colorSL = color.slice(-6);
        if (opacity.length>7) {
          var merge = opacitySL + colorSL
        } else {
          var merge = color
        }
        return merge
     }
}
