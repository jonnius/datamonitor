import QtQuick 2.4

Rectangle {
    id: colorRect
    width: 100
    height: 100
    antialiasing: false
    property string namePalette
    property string colorSelectedRect:"transparent"
    property string colorBorderSelectedRect:"transparent"
    property string colorTracker
    property string borderColorTracker

    signal resetColorSelection()

    MouseArea {
        anchors.fill: parent
        onClicked: {
            resetColorSelection()
            colorSelectedRect=colorTracker
            colorBorderSelectedRect=borderColorTracker
            switch (namePalette) {
            case "background": settings.backgrndColorCust=colorRect.color;
                         break;
            case "grid": settings.gridColorCust=colorRect.color;
                         break;
            case "font": settings.fontColorCust=colorRect.color;
                         break;
            case "line": settings.lineColorCust=colorRect.color;
                         break;
            case "button": settings.buttonColorCust=colorRect.color;
                         break;
            case "bar": settings.barColorCust=colorRect.color;
                         break;
            }
        }
    }

    Rectangle {
        id: selectedRect
        anchors.verticalCenter: colorRect.verticalCenter
        anchors.horizontalCenter: colorRect.horizontalCenter
        width: colorRect.width*0.3
        height: colorRect.width*0.3
        radius: height/2
        rotation: 45
        color: colorSelectedRect
        border.color: colorBorderSelectedRect
        border.width: 3
    }
}
